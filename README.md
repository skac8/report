알서포트 과제
=========
---
### Project 설명
- **개발환경:** `macOS`, `IntelliJ`
- **health:** `http://localhost:8080/actuator/health`
- **swagger:** `http://localhost:8080/swagger-ui/index.html`
- **database initialize data:** `h2-data.sql`
- **file path:** `files/attach/`
- **module 설명:**
    - `domain` entity, repository 등 Database와의 통신을 위한 기능을 정의
    - `api` Controller, Service 등 실서비스 구현
- **사용기술:**

| no | name                     |
|:---:|:-------------------------|
| 1  | `JAVA11`                   |
| 2  | `SpringBoot 2.3.2.RELEASE` |
| 3  | `H2`                       |
| 4  | `JPA`                      |
| 5  | `QueryDSL`                 |
| 6  | `Swagger 2`                |
| 7  | `gradle 6.7.1`             |
| 8  | `Bitbucket`                |

---
### IDE 실행
- QueryDSL Q Class 생성
    - ```./gradlew clean build```
- source forder 추가
    - domain/src/main/generated 폴더 마우스 오른쪽 버튼
    - Mark directory As → Generated Source Root
- ApiApplication 실행
    - spring active profiles: ```local```로 설정
    ![setting](/files/readme/setting.png)

---    
### jar 실행
- project build
    - ```./gradlew clean build```
- jar 실행
    - build 완료되어 생성된 jar 파일 directory 이동(report/api/build/libs/)
    - ```java -Dspring.profiles.active=local -jar api.jar```

---
### API 설명
- **Notice 리스트 조회:** `GET /report/notic/search/list`
- **Notice 상세 조회:** `GET /report/notic/search/detail/{id:[\d]+}`
- **Notice 저장:** `POST /report/notice/save`
- **Notice 수정:** `POST /report/notice/modify/{id:[\d]+}`
- **Notice 삭제:** `DELETE /report/notice/delete/{id:[\d]+}`

---
### H2 Web Console
- **web console:** `http://localhost:8080/h2`
- **JDBC URL:** `jdbc:h2:mem:api;DB_CLOSE_ON_EXIT=FALSE;DB_CLOSE_DELAY=-1;`
- **User Name:** `user`
- **Password:** `pass`
![h2 web console](/files/readme/h2_web_console.png)
