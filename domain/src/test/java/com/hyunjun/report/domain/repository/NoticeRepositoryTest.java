package com.hyunjun.report.domain.repository;

import com.hyunjun.report.domain.entity.Notice;
import com.hyunjun.report.domain.entity.QNotice;
import com.querydsl.core.types.Predicate;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@Slf4j
@DataJpaTest
@RunWith(SpringRunner.class)
@ActiveProfiles("domain-local")
public class NoticeRepositoryTest {

    @Autowired
    NoticeRepository noticeRepository;

    @Test
    public void findAllTest() {
        List<Notice> noticeList = noticeRepository.findAll();
        Assert.assertEquals("notice total count not valid", 15, noticeList.size());
    }

    @Test
    public void findOneTest() {
        Long id = 1L;
        Boolean isActive = true;
        String user = "user_1";

        QNotice qNotice = QNotice.notice;
        Predicate predicate = qNotice
                .id.eq(id)
                .and(qNotice.user.eq(user))
                .and(qNotice.isActive.eq(isActive));

        Notice notice = noticeRepository.findOne(predicate).orElse(null);

        Assert.assertNotNull("notice is null", notice);
    }

    @Test
    public void findByIdAndIsActiveEqualsTest() {
        Long id = 1L;
        Boolean isActive = true;

        Notice notice = noticeRepository.findByIdAndIsActiveEquals(id, isActive).orElse(null);

        Assert.assertNotNull("notice is null", notice);
    }
}
