package com.hyunjun.report.domain;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.hyunjun.report")
public class DomainApplicationTest {

}
