package com.hyunjun.report.domain.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "noticeAttach")
public class NoticeAttach {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @NotNull
    @JsonBackReference
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "noticeId", referencedColumnName = "id")
    Notice notice;

    @Size(max = 100)
    @JsonProperty("fileName")
    String originFileName;

    @Size(max = 48)
    @JsonIgnore
    String savedFileName;

}
