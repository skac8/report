package com.hyunjun.report.domain.repository;

import com.hyunjun.report.domain.entity.Notice;
import com.hyunjun.report.domain.entity.NoticeAttach;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

@Validated
@Repository
public interface NoticeAttachRepository extends JpaRepository<NoticeAttach, Long>, QuerydslPredicateExecutor<NoticeAttach> {

    void deleteAllByNotice(@NotNull Notice notice);

}
