package com.hyunjun.report.domain.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.LinkedList;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "notice")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Notice extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @NotNull
    @Column(columnDefinition = "varchar(200) not null comment '제목'")
    String title;

    @NotNull
    @Column(columnDefinition = "varchar(1000) not null comment '내용'")
    String contents;

    @NotNull
    @Builder.Default
    @Column(columnDefinition = "tinyint(1) not null default '1' comment '글 상태'")
    Boolean isActive = true;

    @NotNull
    @Column(columnDefinition = "varchar(50) not null comment '내용'")
    String user;

    @Builder.Default
    @JsonManagedReference
    @OneToMany(mappedBy = "notice")
    List<NoticeAttach> noticeAttaches = new LinkedList<>();

}
