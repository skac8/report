package com.hyunjun.report.api.common.exception;

import lombok.Getter;

public class NotFoundNoticeException extends RuntimeException {

    @Getter
    private final Long id;

    public NotFoundNoticeException(Long id) {
        super("notice not found");
        this.id = id;
    }
}
