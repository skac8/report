package com.hyunjun.report.api.common.exception.handler;

import com.hyunjun.report.api.common.exception.NotFoundNoticeException;
import com.hyunjun.report.api.model.CommonResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@ControllerAdvice
public class NoticeControllerAdvice {

    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public CommonResponse<Void> handleException(Exception e) {
        log.error("[common] unexpected error: {}", e.getMessage(), e);
        return CommonResponse.<Void>builder()
                .code(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .message(String.format("server error: %s", e.getMessage()))
                .build();
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public CommonResponse<Void> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        List<String> messages = new ArrayList<>();

        if (e.getBindingResult().hasErrors()) {
            for (FieldError fieldError : e.getBindingResult().getFieldErrors()) {
                messages.add(fieldError.getField() + ": " + fieldError.getDefaultMessage());
            }
        }

        log.warn("[common] method argument not valid: ", e);
        return CommonResponse.<Void>builder()
                .code(HttpStatus.BAD_REQUEST.value())
                .message(String.format("bad request: %s", messages))
                .build();
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(NotFoundNoticeException.class)
    public CommonResponse<Void> notFoundNoticeException(NotFoundNoticeException e) {
        log.warn("[common] not found notice: {}", e.getMessage(), e);
        return CommonResponse.<Void>builder()
                .code(HttpStatus.NOT_FOUND.value())
                .message("not found notice")
                .build();
    }
}
