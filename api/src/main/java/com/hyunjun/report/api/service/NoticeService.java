package com.hyunjun.report.api.service;

import com.hyunjun.report.api.common.exception.NotFoundNoticeException;
import com.hyunjun.report.api.common.value.SortOrder;
import com.hyunjun.report.api.model.NoticeDto;
import com.hyunjun.report.api.model.NoticeModifyDto;
import com.hyunjun.report.api.model.NoticeSearchDto;
import com.hyunjun.report.domain.entity.Notice;
import com.hyunjun.report.domain.entity.NoticeAttach;
import com.hyunjun.report.domain.entity.QNotice;
import com.hyunjun.report.domain.repository.NoticeAttachRepository;
import com.hyunjun.report.domain.repository.NoticeRepository;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.dsl.BooleanExpression;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.h2.store.fs.FileUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class NoticeService {

    private final NoticeRepository noticeRepository;
    private final NoticeAttachRepository noticeAttachRepository;
    private final DateTimeFormatter fileNameSuffixFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmssS");
    private final String attachFilePath = System.getProperty("user.dir") + "/files/attach/";

    /**
     * 상세 조회
     *
     * @param id Notice ID
     * @return Notice
     */
    public Notice getNotice(Long id) {
        return noticeRepository.findByIdAndIsActiveEquals(id, true).orElseThrow(() -> new NotFoundNoticeException(id));
    }

    /**
     * 리스트 조회(pagination)
     *
     * @param noticeSearchDto Notice 조회 모델
     * @return Page<Notice>
     */
    public Page<Notice> getNotices(NoticeSearchDto noticeSearchDto) {
        Sort sort = Sort.by(noticeSearchDto.getSort().name().toLowerCase());

        BooleanBuilder booleanBuilder = new BooleanBuilder()
                .and(QNotice.notice.isActive.eq(true))
                .and(likeTitle(noticeSearchDto.getTitle()))
                .and(likeContents(noticeSearchDto.getContents()))
                .and(likeUser(noticeSearchDto.getUser()));

        return noticeRepository.findAll(
                booleanBuilder,
                PageRequest.of(noticeSearchDto.getPage(),
                        noticeSearchDto.getSize(),
                        noticeSearchDto.getSortOrder().equals(SortOrder.ASC) ? sort : sort.descending()
                ));
    }

    /**
     * 저장
     *
     * @param noticeDto 공지
     * @param files     첨부 파일
     * @return Notice
     */
    public Notice saveNotice(NoticeDto noticeDto, List<MultipartFile> files) {

        Notice notice = noticeRepository.save(Notice.builder()
                .title(noticeDto.getTitle())
                .contents(noticeDto.getContents())
                .user(noticeDto.getUser())
                .build());

        saveAttachFiles(notice, files);
        return notice;
    }

    /**
     * 첨부 파일 저장(file, db)
     *
     * @param notice 공지
     * @param files  첨부 파일
     */
    private void saveAttachFiles(Notice notice, List<MultipartFile> files) {
        List<NoticeAttach> noticeAttaches = new LinkedList<>();
        String now = ZonedDateTime.now().format(fileNameSuffixFormatter);

        for (MultipartFile file : files) {
            String originFileName = Objects.requireNonNull(file.getOriginalFilename());
            String savedFileName = now + "_" + UUID.randomUUID().toString().replaceAll("([-])+", "");
            FileUtils.createFile(attachFilePath + savedFileName);
            noticeAttaches.add(NoticeAttach.builder()
                    .notice(notice)
                    .originFileName(originFileName)
                    .savedFileName(savedFileName)
                    .build());
        }

        noticeAttachRepository.saveAll(noticeAttaches);
        notice.getNoticeAttaches().addAll(noticeAttaches);
    }

    /**
     * 수정
     *
     * @param id              Notice ID
     * @param noticeModifyDto 요청 DTO
     * @param files           첨부파일
     * @return Notice
     */
    public Notice modifyNotice(Long id, NoticeModifyDto noticeModifyDto, List<MultipartFile> files) {
        Notice notice = getNotice(id);

        if (!notice.getTitle().equals(noticeModifyDto.getTitle()))
            notice.setTitle(noticeModifyDto.getTitle());

        if (!notice.getContents().equals(noticeModifyDto.getContents()))
            notice.setTitle(noticeModifyDto.getContents());

        if (!files.isEmpty()) {
            // 파일 삭제
            deleteSavedAttachFiles(notice);
            noticeAttachRepository.deleteAllByNotice(notice);
            notice.getNoticeAttaches().clear();

            // 파일 저장
            saveAttachFiles(notice, files);
        }
        return noticeRepository.save(notice);
    }

    /**
     * 삭제 (history 관리를 위해 status update)
     *
     * @param id Notice ID
     */
    public void deleteNotice(Long id) {
        Notice notice = getNotice(id);
        notice.setIsActive(false);
        deleteSavedAttachFiles(notice);
        noticeRepository.save(notice);
    }

    /**
     * 첨부 파일 삭제
     *
     * @param notice Notice
     */
    private void deleteSavedAttachFiles(Notice notice) {
        for (NoticeAttach noticeAttach : notice.getNoticeAttaches()) {
            FileUtils.delete(attachFilePath + noticeAttach.getSavedFileName());
        }
    }

    private BooleanExpression likeTitle(String title) {
        return StringUtils.isEmpty(title) ? null : QNotice.notice.title.contains(title);
    }

    private BooleanExpression likeContents(String contents) {
        return StringUtils.isEmpty(contents) ? null : QNotice.notice.contents.contains(contents);
    }

    private BooleanExpression likeUser(String user) {
        return StringUtils.isEmpty(user) ? null : QNotice.notice.user.contains(user);
    }
}
