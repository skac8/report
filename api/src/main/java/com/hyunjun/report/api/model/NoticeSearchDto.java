package com.hyunjun.report.api.model;

import com.hyunjun.report.api.common.value.NoticeSort;
import com.hyunjun.report.api.common.value.SortOrder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class NoticeSearchDto extends NoticeDto {

    Integer page = 0;

    Integer size = 5;

    NoticeSort sort = NoticeSort.ID;

    SortOrder sortOrder = SortOrder.ASC;

}
