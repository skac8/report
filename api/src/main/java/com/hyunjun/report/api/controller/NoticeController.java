package com.hyunjun.report.api.controller;

import com.hyunjun.report.api.model.CommonResponse;
import com.hyunjun.report.api.model.NoticeDto;
import com.hyunjun.report.api.model.NoticeModifyDto;
import com.hyunjun.report.api.model.NoticeSearchDto;
import com.hyunjun.report.api.service.NoticeService;
import com.hyunjun.report.domain.entity.Notice;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/report/notice")
public class NoticeController {

    private final NoticeService noticeService;

    @ApiOperation(value = "상세 조회")
    @GetMapping("/search/detail/{id:[\\d]+}")
    public CommonResponse<Notice> getNotice(@ApiParam("Notice ID") @PathVariable Long id) {
        Notice notice = noticeService.getNotice(id);
        return CommonResponse.<Notice>builder()
                .data(notice)
                .build();
    }

    @ApiOperation(value = "리스트 조회")
    @GetMapping("/search/list")
    public CommonResponse<List<Notice>> getBoardList(NoticeSearchDto pageInfoDto) {
        Page<Notice> notices = noticeService.getNotices(pageInfoDto);
        return CommonResponse.<List<Notice>>builder()
                .data(notices.getContent())
                .totalPages(notices.getTotalPages())
                .totalElements(notices.getTotalElements())
                .build();
    }

    @ApiOperation(value = "저장")
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "/save", produces = "application/json", consumes = "multipart/form-data")
    public CommonResponse<Notice> saveNotice(@ModelAttribute NoticeDto noticeDto,
                                             @RequestPart("files") List<MultipartFile> files) {
        Notice notice = noticeService.saveNotice(noticeDto, files);
        return CommonResponse.<Notice>builder()
                .data(notice)
                .build();
    }

    // PATCH method에서 multipart/form-data 이슈가 있어 POST로 구현
    @ApiOperation(value = "수정")
    @PostMapping(value = "/modify/{id:[\\d]+}", produces = "application/json", consumes = "multipart/form-data")
    public CommonResponse<Notice> modifyNotice(@PathVariable Long id,
                                               @ModelAttribute NoticeModifyDto noticeModifyDto,
                                               @RequestPart("files") List<MultipartFile> files) {
        Notice notice = noticeService.modifyNotice(id, noticeModifyDto, files);
        return CommonResponse.<Notice>builder()
                .data(notice)
                .build();
    }

    @ApiOperation(value = "삭제")
    @DeleteMapping("/delete/{id:[\\d]+}")
    public CommonResponse<Void> deleteNotice(@PathVariable Long id) {
        noticeService.deleteNotice(id);
        return CommonResponse.<Void>builder()
                .message("delete successfully")
                .build();
    }
}
