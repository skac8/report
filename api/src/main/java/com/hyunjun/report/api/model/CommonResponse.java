package com.hyunjun.report.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;

@Getter
@Setter
@Builder
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CommonResponse<T> {

    @Builder.Default
    Integer code = HttpStatus.OK.value();

    String message;

    T data;

    Integer totalPages;

    Long totalElements;

}
