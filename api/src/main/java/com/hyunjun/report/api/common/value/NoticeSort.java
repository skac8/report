package com.hyunjun.report.api.common.value;

public enum NoticeSort {
    ID,
    TITLE,
    CONTENTS,
    IS_ACTIVE,
    USER

}
